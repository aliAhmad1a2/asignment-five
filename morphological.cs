using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Runtime.Intrinsics.X86;
using System.Threading.Channels;

namespace AssignmentFive;

public class morphological
{ 
    public static bool checkGrayScale(Bitmap bmp)
    {
        if (bmp.PixelFormat == PixelFormat.Format1bppIndexed)
        {
            Console.WriteLine(bmp.PixelFormat);
            return true;
        }
        else
        {
            return false;
        }

    }
        
    public static void diliation(Bitmap SrcImage, int[,]sElement) 
    {
    Bitmap tempbmp = new Bitmap(SrcImage.Width, SrcImage.Height, PixelFormat.Format1bppIndexed);

    BitmapData SrcData = SrcImage.LockBits(new Rectangle(0, 0, SrcImage.Width, SrcImage.Height),
                                            ImageLockMode.ReadOnly, PixelFormat.Format1bppIndexed);

    BitmapData DestData = tempbmp.LockBits(new Rectangle(0, 0, tempbmp.Width, tempbmp.Height),
                                            ImageLockMode.ReadWrite, PixelFormat.Format1bppIndexed);
    
    int size = 3;
    byte max;
    int radius = size / 2;
    int ir, jr;

    unsafe
    {
        int srcDataHeight = SrcData.Height - radius;
        int srcDataWidth = SrcData.Width - radius;

        for (int colm = radius; colm <srcDataHeight ; colm++)
        {
            byte* ptr = (byte*)SrcData.Scan0 + (colm * SrcData.Stride);
            byte* dstPtr = (byte*)DestData.Scan0 + (colm * DestData.Stride);

            for (int row = radius; row < srcDataWidth; row++)
            {
                max = 0;
                for (int eleColm = 0; eleColm < 3; eleColm++)
                {
                    ir = eleColm - radius;
                    byte* tempPtr = (byte*)SrcData.Scan0 + ((colm + ir) * SrcData.Stride);

                    for (int eleRow = 0; eleRow < 3; eleRow++)
                    {
                        jr = eleRow - radius;
                        
                        byte tempVal = (byte)((tempPtr[(row + jr) >> 3] >> (7 - ((row + jr) & 7))) & 1);

                        if (max < tempVal && sElement[eleColm, eleRow] != 0)
                            max = tempVal;
                    }
                }

                int bitIndex = row & 7;
                int byteIndex = row >> 3;
                byte bitMask = (byte)(1 << (7 - bitIndex));
                dstPtr[byteIndex] |= (byte)(max << (7 - bitIndex));
            }
        }
    }

    SrcImage.UnlockBits(SrcData);
    tempbmp.UnlockBits(DestData);
    tempbmp.Save("../../../Image/testImage.bmp");
}


    public static void Erosiondif(Bitmap input, int[,] kernel)
    {
        Bitmap output = new Bitmap(input.Width, input.Height, PixelFormat.Format1bppIndexed);
        
        BitmapData inputData = input.LockBits(new Rectangle(0, 0, input.Width, input.Height), ImageLockMode.ReadOnly,
            PixelFormat.Format1bppIndexed);
        BitmapData outputData = output.LockBits(new Rectangle(0, 0, output.Width, output.Height),
            ImageLockMode.WriteOnly, PixelFormat.Format1bppIndexed);


        int inputStride = inputData.Stride;
        int outputStride = outputData.Stride;
        unsafe
        {
            int nHeight = input.Height - 1;
            int nWidth = input.Width - 1;
            for (int y = 1; y <nHeight ; y++)
            {
                byte * inputRow = (byte *)inputData.Scan0 + (y * inputStride);
                byte * outputRow = (byte *)outputData.Scan0 + (y * outputStride);

                for (int x = 1; x < nWidth; x++)
                {

                    int result = 1;
                    for (int i = -1; i <= 1; i++)
                    {
                        byte * row = inputRow + (i * inputStride);
                        for (int j = -1; j <= 1; j++)
                        {
                            byte * pixel = row + ((x + j) >> 3);
                            int bitIndex = 7 - ((x + j) & 7);
                            if (kernel[i + 1, j + 1] == 1)
                            {
                                result &= ((*pixel & (1 << bitIndex)) != 0 ? 1 : 0);
                            }
                        }
                    }

                    byte * outputPixel = outputRow + (x >> 3);
                    int outputBitIndex = 7 - (x & 7);
                    if (result == 1)
                    {
                        *outputPixel |= (byte)(1 << outputBitIndex);
                    }
                    else
                    {
                        *outputPixel &= (byte)~(1 << outputBitIndex);
                    }
                }
            }
        }

        input.UnlockBits(inputData);
        output.UnlockBits(outputData);

        output.Save("../../../Image/erosionImage.bmp");

    }
}