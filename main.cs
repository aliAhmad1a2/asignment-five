﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using AssignmentFive;


class main
{
     public static int[,] sElement = new int[3, 3]
    {
        {  1, 1, 1 },
        {  1, 1, 1 },
        {  1, 1, 1 }
    };
    static void Main(string[] args )
    {
        try
        { 
            Bitmap imageShap = new Bitmap("../../../Image/monoImage.bmp");
            morphological.checkGrayScale(imageShap);
            morphological.diliation(imageShap,sElement);
            morphological.Erosiondif(imageShap,sElement);
        }
        catch (ArgumentException)
        {
            Console.WriteLine("can't read the path ");
            throw;
        }


    }
}